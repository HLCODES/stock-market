<?php
ob_start();



?>
<!DOCTYPE html>
<html>
<head>
  <title>Home | STOCKEX</title>
  <link href="https://fonts.googleapis.com/css?family=Squada+One&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel = "icon" href =  "bt1.png" type = "image/x-icon">  

        <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>

  
body{
  margin: 0;
 
background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(dt.jpg);
background-repeat: no-repeat;
background-size: cover;
 /*  background-color: white;
   */
   
   color: white;
}
html,body{
  height: 100%;
  width: 100%;
}
  
.hdr{
   background-color: black;
   color:white;
   padding: 8px 0px 51px 45px;
   height: 2px;
  
   border: 1px solid red;
   border-left: currentColor;
   border-right: currentColor;
   border-top: currentColor;
   border-width: 3px;
   position: relative;
   z-index: 1;
 }
 header .h-t{
   display: inline;
   font-family: 'Squada One', cursive;
   font-weight: 400;
   font-size: 50px;
   float: left;
   margin-left: 40px;
   margin-top: -5px;
   margin-right: 10px;
   font-color: rgb(255,0,0);
 }
 .lm{
  height: 120px;
  width: 120px;
  position: absolute;
  margin-left: -472px;
  margin-top: -25px;
}
 footer h1{
   margin: 0;
   font-size: 20px;
   margin-top: 10px;
   position: absolute;
   text-align: center;
   margin-left: 825px;
 }

 nav ul{
   display: inline;
   padding: 0px;
   float: left;
 }
 nav ul li{
   display: inline-block;
   list-style-type: none;
   color: white;
   float: left;
   font-family: 'Montserrat', sans-serif;
   margin-left: 14px;
   margin-top: 10px;
   text-decoration: none !important;
 }
 nav ul li a{
   color: white;
   text-decoration: none !important;

 }

a{
  text-decoration: none;
}
 .aboutred{
   background-color: red;
   padding: 30px 10px 20px 10px;

 }
 .divider{
   background-color: red;
   height: 5px;
 }
 .aboutblack:hover{
   
   color: red;
   padding: 30px 0px 20px 0px;

 }
.aboutred:hover{
  color: white;
}

footer{
  position: relative;
  bottom: 0px;
 border: 1px solid red;
border-width: 3px;
border-bottom: currentColor;
border-right: currentColor;
border-left: currentColor;
background-color: rgb(44,37,37);
color: white;
}
footer ul li{
  list-style-type: none;
}
.footer-bottom{
border: 1px solid red;
bottom: 0px;
border-width: 3px;
border-bottom: currentColor;
border-right: currentColor;
border-left: currentColor;
background-color: rgb(34,24,24);
text-align: center;
position:relative;

}

section{
  min-height: 100%;
  top:- 10px;
  position:relative;
   
}



/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */

}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 35%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: grey;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;

  background-color: black;
  color: white;
  border-bottom: 1px solid grey;
  font-family: 'Roboto', sans-serif;
}

.modal-body {padding: 2px 16px;
height: 180px;
font-size: 25px;
font-family: 'Roboto', sans-serif;
/*margin-left: 15px;*/
color: white;
background-color: black;
  border-bottom: 1px solid grey;
}

.modal-footer {
  padding: 2px 16px;
  background-color: black;
  border-bottom: 1px solid black;

}

.con1{
  display: none;
}
#jsk{
  visibility: hidden;
}
#bas{
  display: none;
  margin-left: 30px;
}
#zz{
  display: block;
  margin-left: 30px;
}

#chart_div{

  margin-left: 30px;
  
  position: absolute;
}

#chart_div1{

  margin-left: 30px;
  
  position: absolute;
}
/* Popup container - can be anything you want */
.popup {
  position: relative;
  display: inline-block;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* The actual popup */
.popup .popuptext {
  visibility: hidden;
  width: 160px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 8px 0;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: -80px;
}

/* Popup arrow */
.popup .popuptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup .show {
  visibility: visible;
  -webkit-animation: fadeIn 1s;
  animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
  from {opacity: 0;} 
  to {opacity: 1;}
}

@keyframes fadeIn {
  from {opacity: 0;}
  to {opacity:1 ;}
}


.w3-modal{
  background-color: grey;
  width: 260px;
  margin-left: 500px;
}
</style>
</head>

<body onload="document.getElementById('id01').style.display='block'">
  <header class="hdr">
    <nav>
     <a href="index.php"><img src="bt1.png"  class="lm"> </a> 
     <h1 class="h-t"><a href="home.php" style="text-decoration: none; color: red;">STOCKEX</a></h1>
     <ul id="nav">
       <li><a class="aboutred" href="home.php">MARKET</a></li>
       <li><a class="aboutblack" href="test.php">COMPANIES</a></li>

       
     </ul>
   
   </nav>
   
    
   </header>


<div id="id01" class="modal">
    <div class="modal-content">
      <div class="container">
        <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright"  style='color:black;'>&times;</span>
        <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM nse50  ORDER BY id DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);
                     echo "<h6 style='color:black;'>NIFTY50</h6>";
echo '<h1  style="color:black;"" >'. $row["open"].' </h1><br><p  style="color:black;">Previous close</p> <br><h6  style="color:black;">'. $row["close"].'</h6>';?>

   
      </div>
    </div>
  </div>


<div class="dropdown">
<select id="cars" name="cars" onchange="admSelectCheck(this);">
    <option value="nse" id="1">NSE</option>
    <option value="bse" id="2">BSE</option>
  
  </select>
</div>
<SECTION id="zz">
<div class="con">
  
   <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM nse50  ORDER BY id DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);
                     echo "<h1>NIFTY50</h1>";
echo '<br>Open '. $row["open"].' <br>Previous close '. $row["close"].'<br>Day High '. $row["high"].'<br>Day Low '. $row["low"];?>

 <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM nse50 WHERE high IS NOT NULL ORDER BY high DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);

 $sql1 = "SELECT * FROM nse50 WHERE low IS NOT NULL ORDER BY low ASC;";
                     $result1 = mysqli_query($conn,$sql1);
                     $row1=mysqli_fetch_assoc($result1);

                  
echo '<br>52 Week High '. $row["high"].' <br>52 Week Low '. $row1["low"].'';?>

</div>




<div class="con1">
  
   <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM bse  ORDER BY id DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);
                     echo "<h1>BSE</h1>";
echo '<br>Open '. $row["open"].' <br>Previous close '. $row["close"].'<br>Day High '. $row["high"].'<br>Day Low '. $row["low"];?>

 <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM bse WHERE high IS NOT NULL ORDER BY high DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);

 $sql1 = "SELECT * FROM bse WHERE low IS NOT NULL ORDER BY low ASC;";
                     $result1 = mysqli_query($conn,$sql1);
                     $row1=mysqli_fetch_assoc($result1);

                  
echo '<br>52 Week High '. $row["high"].' <br>52 Week Low '. $row1["low"].'';?>

</div>
 <div id="chart_div" style="width: 95%; height: 500px;"></div>   
</SECTION>




<SECTION id="bas">
<div class="con">
  
   <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM bse  ORDER BY id DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);
                     echo "<h1>BSE</h1>";
echo '<br>Open '. $row["open"].' <br>Previous close '. $row["close"].'<br>Day High '. $row["high"].'<br>Day Low '. $row["low"];?>

 <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM bse WHERE high IS NOT NULL ORDER BY high DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);

 $sql1 = "SELECT * FROM bse WHERE low IS NOT NULL ORDER BY low ASC;";
                     $result1 = mysqli_query($conn,$sql1);
                     $row1=mysqli_fetch_assoc($result1);

                  
echo '<br>52 Week High '. $row["high"].' <br>52 Week Low '. $row1["low"].'';?>

</div>




<div class="con1">
  
   <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM bse  ORDER BY id DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);
                     echo "<h1>BSE</h1>";
echo '<br>Open '. $row["open"].' <br>Previous close '. $row["close"].'<br>Day High '. $row["high"].'<br>Day Low '. $row["low"];?>

 <?php

                      include_once "includes/dbh.inc.php";
                    $sql = "SELECT * FROM bse WHERE high IS NOT NULL ORDER BY high DESC;";
                     $result = mysqli_query($conn,$sql);
                     $row=mysqli_fetch_assoc($result);

 $sql1 = "SELECT * FROM bse WHERE low IS NOT NULL ORDER BY low ASC;";
                     $result1 = mysqli_query($conn,$sql1);
                     $row1=mysqli_fetch_assoc($result1);

                  
echo '<br>52 Week High '. $row["high"].' <br>52 Week Low '. $row1["low"].'';?>

</div>
  <div id="chart_div1" style="width: 95%; height: 500px;"></div>         
</SECTION>


<footer>
<ul class="footer-list" type="none">
 
  <li>Contact Us</li>
  <li>Terms and conditions</li>
  
   
</ul>


<div class="footer-bottom">&copy stockex.com | Designed by Hitesh Lokhande |</div></footer>

    
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
<script type="text/javascript">

function sd(){
  var selectBox = document.getElementById("car");
  var b =selectBox.options(selectBox.selectedIndex).value;
  if(b=='bat')
  { document.getElementById("jsk").style.visibility= visible;}
else
  {document.getElementById("jsk").style.visibility= visible}
return false;
</script>
  

<script type="text/javascript">
  



  function admSelectCheck(nameSelect){
    var admOptionValue = document.getElementById("cars").value;
    if (admOptionValue=="nse") {
    var at = document.getElementById("zz");
    var at1 = document.getElementById("bas");

 
    at.style.display="block";
    at1.style.display="none";
  



  }
    else if (admOptionValue=="bse") {   
    var at = document.getElementById("zz");
    var at1 = document.getElementById("bas");

 
    at1.style.display="block";
    at.style.display="none";

  }}
</script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
         <?php

                      include_once "includes/dbh.inc.php";
                       $sql = "SELECT * FROM nse50  ORDER BY id ";
                      if(isset($_GET['days'])& !empty($_GET['days'])){
                        $sql .= "ASC LIMIT {$_GET['days']}";} else $sql .= "ASC";

                      
                   
                     $result = mysqli_query($conn,$sql);
          

                    while( $row=mysqli_fetch_assoc($result)){ 

    echo '["'.$row["date"].'", '.$row["low"].','.$row["open"].','. $row["close"].','. $row["high"].'],';  }?>
      // Treat first row as data as well.
    ], true);

    var options = {
          legend: 'none',
          bar: { groupWidth: '100%' }, // Remove space between bars.
          candlestick: {
            fallingColor: { strokeWidth: 0, fill: '#a52714' }, // red
            risingColor: { strokeWidth: 0, fill: '#0f9d58' }   // green
          }
        };


    var chart = new google.visualization.CandlestickChart(document.getElementById('chart_div'));

    chart.draw(data, options);
  }
    </script>

      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
         <?php

                      include_once "includes/dbh.inc.php";
                       $sql = "SELECT * FROM bse  ORDER BY id ";
                      if(isset($_GET['days'])& !empty($_GET['days'])){
                        $sql .= "ASC LIMIT {$_GET['days']}";} else $sql .= "ASC";

                      
                   
                     $result = mysqli_query($conn,$sql);
          

                    while( $row=mysqli_fetch_assoc($result)){ 

    echo '["'.$row["date"].'", '.$row["low"].','.$row["open"].','. $row["close"].','. $row["high"].'],';  }?>
      // Treat first row as data as well.
    ], true);

    var options = {
          legend: 'none',
          bar: { groupWidth: '100%' }, // Remove space between bars.
          candlestick: {
            fallingColor: { strokeWidth: 0, fill: '#a52714' }, // red
            risingColor: { strokeWidth: 0, fill: '#0f9d58' }   // green
          }
        };


    var chart = new google.visualization.CandlestickChart(document.getElementById('chart_div1'));

    chart.draw(data, options);
  }
    </script>
<script>
// When the user clicks on div, open the popup
function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
</script>
  </body>


 
  </html>


